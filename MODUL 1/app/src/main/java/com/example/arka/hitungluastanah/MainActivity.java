package com.example.arka.hitungluastanah;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    EditText alas,tinggi;
    Button cek;
    TextView hasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        alas = (EditText) findViewById(R.id.alas);
        tinggi = (EditText) findViewById(R.id.tinggi);
        cek = (Button) findViewById(R.id.cek);
        hasil = (TextView) findViewById(R.id.hasil);

        cek.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String isialas = alas.getText().toString();
                String isitinggi = tinggi.getText().toString();

                double alas = Double.parseDouble(isialas);
                double tinggi = Double.parseDouble(isitinggi);

                double hs = luastanah(alas, tinggi);
                String output = String.valueOf(hs);
                hasil.setText(output.toString());
            }
            });
        }

            public double luastanah ( double alas, double tinggi){
                return (alas * tinggi);
            }

        }
